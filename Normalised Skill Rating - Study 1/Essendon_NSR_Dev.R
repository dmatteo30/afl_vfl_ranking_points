# Calculating Normalised Skill Rating and Age Adjusted Normalised Skill Rating with Champion Data Ranking Points#

# Load packages

library(tidyverse)
library(ggrepel)
library(lme4)
library(blme)
library(cowplot)

# Load Ranking points from 2015 to 2023 for AFL, VFL, WAFL, SANFL
df <- read.csv("Normalised Skill Rating - Study 1/Ranking_Points_15_23.csv")
df$position <- relevel(as.factor(df$position), "Mid") #make mid the reference factor
df <- df %>% 
  distinct(person_id, roundID, .keep_all = TRUE) %>%
  filter(position != " ") #cleaning up duplicates and removing players without positions

# initialise value for loop, starting point must be greater than 2 otherwise model won't fit to singular instances of player
x= 2

# empty list to store data frames created in loop
skill_list <- list()

# loop model for each round
while(x <= length(unique(df$roundID))){
  
  
  
  # create log variables and ID variables
  df_model <- df %>% 
    filter(
      ranking_points > 1  # just filtering out any games with RP < 1 (think of a better thing to do later)
    ) %>%
    mutate(
      ID = paste(fullname, person_id, sep = '_'),
      logrp = log(ranking_points)
    )
  
  # filter round to be current round and before
  df_model <- df_model %>%
    filter(roundID <= x)
  
  # creates data frame of players who show up in future rounds of the data frame, used later to filter out players who no longer play
  future_players <- df %>%
    filter(roundID >= x) %>%
    mutate(
      ID = paste(fullname, person_id, sep = '_')) %>%
    select(ID) %>%
    distinct()
  
  
  # run bayesian model, for frequentist model replace blmer with lmer
  skill_model <- blmer(logrp ~ position + league + (1 | Age) + (1  | ID),
                     data = df_model)
  
  pskills <- as.data.frame(ranef(skill_model)) # get model random effects
  
  ID <- pskills %>%
    filter(grpvar == "ID") %>%
    rename_with(.fn = function(.x){paste0(.x,"_ID")}) # filter pskills to get player random effects
  
  Age <- pskills %>%
    filter(grpvar == "Age") %>%
    rename_with(.fn = function(.x){paste0(.x,"_Age")}) # filter pskills to get age random effects
  
  Age$Age <- as.numeric(as.character(Age$grp_Age)) # force Age as numeric instead of factor 
  
  final_players <- df_model %>%
    filter(roundID == x) # filter df_model to only get player vaules for current round 
  
  
  
  final_players <- left_join(final_players, Age, by = c("Age" = "Age")) # join age random effect to list of players from round
  final_players$age_multiplier <- exp(final_players$condval_Age) # create age multiplier by exponential transformation of age conditional value
  final_players$age_sd <- exp(final_players$condsd_Age) # create age SD by exponential transformation of age conditional SD
  
  
  final_players <- left_join(final_players, ID, by = c("ID" = "grp_ID")) # join player random effect to list of players from round
  final_players$NSR <- exp(final_players$condval_ID) # create Normalised Skill Rating by exponential transformation of player conditional value
  final_players$NSR_sd <- exp(final_players$condsd_ID) # create Normalised Skill Rating SD by exponential transformation of player conditional SD
  
  
  # applying SD as error for Normalised Skill Rating
  final_players$NSR_low <- final_players$NSR - (final_players$NSR_sd-1)
  final_players$NSR_high <- final_players$NSR + (final_players$NSR_sd-1)
  
  
  final_players$AANSR <- final_players$NSR * final_players$age_multiplier # creating Age Adjusted Normalised Skill Rating
  final_players$AANSR_sd <- exp(sqrt(final_players$condsd_ID^2 + final_players$condsd_Age^2)) # creating Age Adjusted Normalised Skill Rating SD
  
  # applying SD as error for Age Adjusted Normalised Skill Rating
  final_players$AANSR_low <- final_players$AANSR -(final_players$AANSR_sd-1)
  final_players$AANSR_high <- final_players$AANSR +(final_players$AANSR_sd-1)
  
  
  end_df <- final_players %>%
    select(-dob, -year, -ID, -logrp, -grpvar_Age, -term_Age, -grp_Age, -condval_Age, -condsd_Age, -grpvar_ID, -term_ID, -condval_ID, -condsd_ID ) %>%
    distinct() #filter out any unneeded columns and make sure no duplicates go to the final data frame
  
  
  
  
  #send to empty list
  skill_list[[x-1]] <- end_df
  #print check
  print(x)
  
  x = x+1
}

#bind data frames in list
NSR_DF <- bind_rows(skill_list)




##### graphing #####
#pick player for graph
graph_player = "Jake Stringer"

#create differences in stats and text placement for annotation
six_week_diff<-NSR_DF%>%
  filter(fullname %in% graph_player) %>%
  summarize(NSRcurrent = nth(NSR,1, order_by = desc(roundID)), 
            NSRprevious =nth(NSR,6, order_by = desc(roundID)),
            NSRdiff = NSRcurrent - NSRprevious,
            AANSRcurrent = nth(AANSR,1, order_by = desc(roundID)), 
            AANSRprevious =nth(AANSR,6, order_by = desc(roundID)),
            AANSRdiff = AANSRcurrent - AANSRprevious,
            text_placement = median(roundID))


# AANSR plot
  plot1 <- NSR_DF%>%
  filter(fullname %in% graph_player) %>%
  ggplot(data=., aes(x=roundID, y= AANSR, colour = fullname)) + geom_point() + geom_line() +
  geom_ribbon(aes(ymin=AANSR_low, ymax=AANSR_high), linetype=2, alpha=0.1)+
  ylim(0,3) +
  annotate(geom="text", x=six_week_diff$text_placement, y=2.7, label= paste0("AANSR change for ", graph_player,"\n", "in the last 6 weeks is:  ",round(six_week_diff$AANSRdiff,3)),
           color="black", size = 5)+
  labs(colour = "Player",
       title = paste0("Age Adjusted Normalised Skill Rating: ", graph_player),
       y = "AANSR",
       x = "sequential Round Number")+
  theme(legend.position="none")



# NSR Plot 
plot2 <-  NSR_DF%>%
  filter(fullname %in% graph_player) %>%
  ggplot(data=., aes(x=roundID, y= NSR, colour = fullname)) + geom_point() + geom_line() +
  geom_ribbon(aes(ymin=NSR_low, ymax=NSR_high), linetype=2, alpha=0.1)+
  ylim(0,3) +
  annotate(geom="text", x=six_week_diff$text_placement, y=2.7, label= paste0("NSR change for ", graph_player,"\n", "in the last 6 weeks is:  ",round(six_week_diff$NSRdiff,3)),
           color="black", size = 5)+
  labs(colour = "Player",
       title = paste0("Normalised Skill Rating: ", graph_player),
       y = "NSR",
       x = "sequential Round Number")+
  theme(legend.position="none")



final_plot<-plot_grid(plot1, plot2)

final_plot

#automatically saves plots to file path (file path will be updating when using new PC)
mypath <- file.path("C:","Users","Dave","Desktop","Plots",paste(today()," ", graph_player, ".jpg", sep = ""))

jpeg(file=mypath, height = 1080, width = 1920, units = "px")
final_plot
dev.off()
