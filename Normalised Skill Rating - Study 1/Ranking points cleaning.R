library(dplyr)
library(tidyverse)
###### clean and compile AFL Data 2012-2023 ######

afl_list <- list()
year = 2015
x = 1
profiles <- read.csv("AFLProfiles.csv")

profiles <- profiles %>%
  filter(SEASON_ID >= 2015) %>%
  select(SEASON_ID, FULLNAME, SQUAD_NAME, CDPOSITION, DOB) %>%
  mutate_at(c("SEASON_ID"), as.character)

#loop through AFL CSVs, clean and add to empty list
while( year < 2024){
season <- read.csv(paste("player",year,".csv", sep = ""))

season <- season %>%  
  select(MATCH_ID, SEASON_ID, GROUP_ROUND_NO, PERSON_ID, FULLNAME, SQUAD_NAME,RANKING_PTS) %>%
  mutate_at(c("MATCH_ID", "SEASON_ID","GROUP_ROUND_NO", "PERSON_ID"), as.character) %>%
  group_by(SEASON_ID,GROUP_ROUND_NO,  PERSON_ID, FULLNAME, SQUAD_NAME) %>%
  summarise(across(where(is.numeric), ~ sum(.x, na.rm = TRUE)))

season <- left_join(season, profiles, by = c("SEASON_ID", "FULLNAME", "SQUAD_NAME")) %>%
  drop_na()

afl_list[[x]] <- season
print(year)

year = year + 1
x = x + 1

}

afl_data <- bind_rows(afl_list)
afl_data$league <- "AFL"

AFL <- afl_data %>%
  rename(season_id = SEASON_ID,
         round = GROUP_ROUND_NO,
         person_id = PERSON_ID,
         fullname = FULLNAME,
         squad_name = SQUAD_NAME,
         ranking_points = RANKING_PTS,
         position = CDPOSITION,
         dob = DOB)

AFL$dob <- as.character(lubridate::dmy(AFL$dob))

###### clean and compile VFL Data 2012-2023 ######
profiles <- read.csv("CHAMPION_ID.csv")

profiles <- profiles %>%
  filter(season_id >= 2015) %>%
  select(season_id, fullname, squad_name, position, dob) %>%
  mutate_at(c("season_id"), as.character)

season <- read.csv("VFL.csv")
  
season <- season %>%
  filter(season_id >=2015) %>%
    select(season_id, round, person_id, fullname, squad_name,ranking_points) %>%
    mutate_at(c("season_id","round", "person_id"), as.character) %>%
    group_by(season_id, round,  person_id, fullname, squad_name) %>%
    summarise(across(where(is.numeric), ~ sum(.x, na.rm = TRUE)))
  
VFL <- left_join(season, profiles, by = c("season_id", "fullname", "squad_name")) %>%
    drop_na()
  
VFL$league <- "VFL"


##### clean and compile SANFL Data 2012-2023 #####
profiles <- read.csv("CHAMPION_ID.csv")

profiles <- profiles %>%
  filter(season_id >= 2012) %>%
  select(season_id, fullname, squad_name, position, dob) %>%
  mutate_at(c("season_id"), as.character)

season <- read.csv("SANFL.csv")

season <- season %>%  
  select(season_id, round, person_id, fullname, squad_name,ranking_points) %>%
  mutate_at(c("season_id","round", "person_id"), as.character) %>%
  group_by(season_id, round,  person_id, fullname, squad_name) %>%
  summarise(across(where(is.numeric), ~ sum(.x, na.rm = TRUE)))

SANFL <- left_join(season, profiles, by = c("season_id", "fullname", "squad_name")) %>%
  drop_na()

SANFL$league <- "SANFL"



##### clean and compile WAFFL Data 2012-2023 #####
profiles <- read.csv("CHAMPION_ID.csv")

profiles <- profiles %>%
  filter(season_id >= 2012) %>%
  select(season_id, fullname, squad_name, position, dob) %>%
  mutate_at(c("season_id"), as.character)

season <- read.csv("WAFL.csv")

season <- season %>%  
  select(season_id, round, person_id, fullname, squad_name,ranking_points) %>%
  mutate_at(c("season_id","round", "person_id"), as.character) %>%
  group_by(season_id, round,  person_id, fullname, squad_name) %>%
  summarise(across(where(is.numeric), ~ sum(.x, na.rm = TRUE)))

WAFL <- left_join(season, profiles, by = c("season_id", "fullname", "squad_name")) %>%
  drop_na()

WAFL$league <- "WAFL"

##### combine all data ##### 
all_data <- bind_rows(AFL, VFL, SANFL, WAFL)

all_data$year <- lubridate::ymd(all_data$season_id, truncated = 2L)


#create Age
all_data$Age <- as.integer(difftime(all_data$year, all_data$dob, units = "weeks")/52)
all_data$round <- as.numeric(all_data$round)
all_data$season_id <- as.numeric(all_data$season_id)
all_data <- all_data %>%
  filter(Age > 17 & Age < 35)

all_data <- all_data %>%
  filter(round > 0) %>%
  arrange(season_id, round)


# create continous round ID
all_data <- all_data %>% 
  group_by(season_id, round) %>%
  mutate(roundID = cur_group_id()) %>%
    ungroup()


write.csv(all_data, "Ranking_Points_15_23.csv", row.names = F)
